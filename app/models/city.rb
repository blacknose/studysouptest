class City < ApplicationRecord
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  belongs_to :county


  settings index: { number_of_shards: 1 } do
    mappings dynamic: 'false' do
      indexes :name, analyzer: 'english', index_options: 'offsets'
    end
  end



  def as_indexed_json(options={})
    hash = as_json(
        only: [:id, :name, :lat, :long],
        include: {
            county: {
                only: [:id, :name],
                include: [:state]
            }
        }
    )
  end

end
