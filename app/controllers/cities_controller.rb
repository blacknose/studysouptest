class CitiesController < ApplicationController
  def search
    @results = City.search(params[:q])
    respond_to do |format|
      format.json { render :json => @results.records.to_json }
    end
  end


  def index
    @query=params[:q]
    @results = City.search(@query)
    respond_to do |format|
      format.json { render json: @results.to_json }
    end
  end
end
