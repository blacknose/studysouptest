# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'http'

states = JSON.parse HTTP.get("https://gist.githubusercontent.com/mshafrir/2646763/raw/8b0dbb93521f5d6889502305335104218454c2bf/states_hash.json").body
states.each do |state|
  lastCounty = ''
  p "Inserting state: #{state}"
  createdState = State.find_or_create_by(name: state[1])
  response = HTTP.get("http://api.sba.gov/geodata/city_links_for_state_of/#{state[0]}.json")
  if response.status.to_i == 200
    cities =  JSON.parse response.body
    countyRecord = nil
    cities.each do | city |
      county = city["full_county_name"]
      unless county == lastCounty
        p "Inserting couty: #{county}"
        countyRecord = County.find_or_create_by(name: county, state: createdState)
      end

      unless countyRecord.nil?
        p "Inserting city: #{city["name"]}"
        City.find_or_create_by(name: city["name"], county: countyRecord)
      end

      lastCounty = county
    end
  end

end

