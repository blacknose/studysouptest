class AddLatAndLongToCity < ActiveRecord::Migration[5.0]
  def change
    add_column :cities, :lat, :decimal
    add_column :cities, :long, :decimal
  end
end
